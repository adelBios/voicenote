//
//  AudioFile.swift
//  VoiceNote
//
//  Created by Adel Radwan on 3/3/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import RealmSwift
import UIKit


@objcMembers class AudioFile:Object {
    
    dynamic var parentId    : String = ""
    dynamic var id          : String = ""
    dynamic var filePath    : String = ""
    dynamic var createdAt   : Date   = Date()
    
    var soundData = List<SoundData>()
    
    convenience init(filePath:String,parentId:String) {
        self.init()
        
        self.id = UUID().uuidString
        self.filePath = filePath
        self.parentId = parentId
        
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
}
