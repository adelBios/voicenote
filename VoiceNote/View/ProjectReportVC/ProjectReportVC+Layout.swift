//
//  ProjectReportVC+Layout.swift
//  Samam
//
//  Created by adel radwan on 11/8/18.
//  Copyright © 2018 adel radwan. All rights reserved.
//

import UIKit

extension ProjectReportVC {
    
    func setupUI(){
        self.backgroundColor = .white
    
        
        self.addSubview(self.stackView)
        self.stackView.snp.makeConstraints {
            $0.leading.trailing.bottom.equalTo(self)
            $0.height.equalTo(40)
        }
        
        self.addSubview(self.reportTextView)
        self.reportTextView.snp.makeConstraints {
            $0.leading.trailing.top.equalTo(self).inset(8)
            $0.bottom.equalTo(self.stackView.snp.top).inset(-8)
        }
        
        self.addSubview(self.reportLabel)
        self.reportLabel.snp.makeConstraints {
            $0.leading.trailing.top.equalTo(reportTextView).inset(8)
            $0.height.equalTo(24)
        }
    
    }
    

    override func layoutSubviews() {
        super.layoutSubviews()
        self.layoutIfNeeded()
       
    }
    
}
