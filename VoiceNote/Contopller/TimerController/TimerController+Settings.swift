//
//  TimerController+MenuOption.swift
//  VoiceNote
//
//  Created by Adel Radwan on 2/20/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import UIKit


extension TimerController {
    
    
    func setupViews(){
        view.addSubview(self.collectionView)

        self.collectionView.snp.makeConstraints({
            $0.leading.trailing.equalTo(self.view)
            $0.top.equalTo(self.view.snp.top)
            $0.height.equalTo(80)
        })
        
    }
    
    
    func colorMenuOption(){
        
        let arrowMenuViewModel0 = DPArrowMenuViewModel(title: "red color (default)",
                                                       imageName: "red")

        let arrowMenuViewModel1 = DPArrowMenuViewModel(title: "orange color",
                                                       imageName: "orange")
        
        let arrowMenuViewModel2 = DPArrowMenuViewModel(title: "yellow color",
                                                       imageName: "yellow")
        
        viewModels.append(arrowMenuViewModel0)
        viewModels.append(arrowMenuViewModel1)
        viewModels.append(arrowMenuViewModel2)
        
    }
    
    
}
