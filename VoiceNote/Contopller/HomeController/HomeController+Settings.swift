//
//  HomeController+Layout.swift
//  VoiceNote
//
//  Created by Adel Radwan on 3/3/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import UIKit


extension HomeController : UICollectionViewDelegateFlowLayout{
    
    func setupViews(){
        self.view.addSubview(self.colletionView)
        self.colletionView.snp.makeConstraints {
            $0.edges.equalTo(view)
        }

    }
    
    
    
    func updateRealm(){
        self.notificationToken = RealmService.shared.updateUI {
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                self.colletionView.reloadData()
            }
        }
    }
    
    func stopRealmUpdate(){
        if let not = self.notificationToken {
            not.invalidate()
        }
    }
    
    func setupNavigationItem(){
        self.navigationItem.title = "add folder".uppercased()
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.addNewFolder))
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.width / 2) - 15, height: 120)
    }
    
    
}
