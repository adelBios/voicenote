//
//  HomeController+Protocol.swift
//  VoiceNote
//
//  Created by Adel Radwan on 3/3/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import UIKit

extension HomeController: UICollectionViewDataSource,UICollectionViewDelegate{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.folderData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! FolderCell
        
        let data = self.folderData[indexPath.row]
        
        cell.configData(folderName: data.folderName)
        cell.didSelectFolderSettigs = {
            self.showActionSheet(deleteFolder: {
                RealmService.shared.delete(data)
            }, changeFolderName: {
                self.addOrUpdateFolderName(text: data.folderName, completion: { name in
                    RealmService.shared.update(data, dictinary: ["folderName":name])
                })
            })
            
        }
        
        return cell
    }
    

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let vc = RecorderController()
        vc.folderId = self.folderData[indexPath.row].id
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        
    }
    

    
}
