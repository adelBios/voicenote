//
//  RealmService.swift
//  Tgmeaat
//
//  Created by Adel Radwan on 2/11/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import RealmSwift

class RealmService {
    
    
    private init(){}
    static let shared = RealmService()
    let realm = try! Realm(configuration: Realm.Configuration(schemaVersion:33))
    
    func updateUI(_ completion:@escaping()->Void)->NotificationToken{
        var token : NotificationToken!
        token = self.realm.observe({ (not, r) in
            completion()
        })
        return token
    }
}

//MARK: - create Objct
extension RealmService {
    
    func create<T:Object>(_ object:T){
        do{
            try realm.write{
                realm.add(object)
            }
        }catch{
            self.postError(error)
        }
    }
    
    func createUpdate<T:Object>(_ object:T){
        do{
            try realm.write{
                realm.add(object, update: true)
            }
            
        }catch{
            self.postError(error)
        }
    }
    
}

//MARK: - delete object
extension RealmService {
    
    
    func delete<T:Object>(_ object:T){
        do{
            try realm.write {
                realm.delete(object)
            }
        }catch{
            self.postError(error)
        }
    }
}

//MARK: - update object

extension RealmService {
    
    func update <T:Object>(_ object:T,dictinary:[String:Any]){
        
        do{
            try realm.write {
                for (key,value) in dictinary {
                    object.setValue(value, forKey: key)
                }
            }
        }catch{
            self.postError(error)
        }
        
    }
    

}


//MARK - handleError
extension RealmService {
    
    private func postError(_ error:Error?){
        NotificationCenter.default.post(name: NSNotification.Name("RealmError"), object: error)
    }
    
    func addObserving(in vc : UIViewController,completion : @escaping (Error?)->Void){
        NotificationCenter.default.addObserver(forName: NSNotification.Name("RealmError"),
                                               object: nil, queue: nil) { not in
                                                completion(not.object as? Error)
        }
    }
    
    func removeObserv(in vc : UIViewController){
        NotificationCenter.default.removeObserver(vc, name: NSNotification.Name("RealmError"), object: nil)
    }
    
}

//MARK: - helper for realmService
extension Object {
    
    func saveToRealm(){
        RealmService.shared.createUpdate(self)
    }
    
    func saveToRealmWithUpdate(){
        RealmService.shared.createUpdate(self)
    }
    
    func remove(){
        RealmService.shared.delete(self)
    }
}

//MARK: - this extension for delete enter data of Model
extension Realm {
    
    func deleteObject(model : Object.Type){
        let realm = RealmService.shared.realm
        let obj = realm.objects(model)
        try! realm.write({
            realm.delete(obj)
        })
    }
}
