//
//  TimerController+protocol.swift
//  VoiceNote
//
//  Created by Adel Radwan on 2/20/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import UIKit

extension TimerController : UICollectionViewDataSource,UICollectionViewDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.timeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! VoiceNoteCell
        let data = self.timeArray[indexPath.row]
        cell.timeLabel.text = data.time
        
        if data.isMarked == true {
            cell.noteImageView.isHidden = false
        }else{
            cell.noteImageView.isHidden = true
        }
        
        cell.timerView.backgroundColor = Constant.getHighlight(Constant.HighlightColor(rawValue: data.higlightedColor) ?? .redColor)
        
        cell.didSelectDisplayNote = {
            self.noteView.show()
            self.noteView.isUpdateNoteView = true
            self.noteView.reportTextView.text = data.note[indexPath.section].note
            self.noteView.textViewDidChange(self.noteView.reportTextView)
            self.noteView.second = data.time

        }

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = self.timeArray[indexPath.row]
        
       try! RealmService.shared.realm.write {
            data.higlightedColor = Constant.highlightColor.rawValue
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        if indexPath.row == timeArray.count - 1 {
//            self.isAutoScroll = true
//        }else{
//            self.isAutoScroll = false
//        }
    }
    
}
