//
//  VoiceNoteCell.swift
//  VoiceNote
//
//  Created by Adel Radwan on 2/20/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import UIKit

class VoiceNoteCell : UICollectionViewCell {
    
    var didSelectDisplayNote : (()->())?
    
    var timerView:UIView = {
        let v = UIView()
        v.backgroundColor = UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.0)
        return v
    }()
    
    private var baseTimeLineView:UIView = {
        let v = UIView()
        v.backgroundColor = .blueColor
        return v
    }()
    var timeLabel : UILabel = {
        let l = UILabel()
        l.textAlignment = .left
        l.font = UIFont.systemFont(ofSize: 10)
        l.textColor = .blueColor
        return l
    }()
    
   lazy var noteImageView : UIButton = {
       let i = UIButton(type: .system)
        i.setImage(#imageLiteral(resourceName: "marker"), for: .normal)
        i.tintColor = .blueColor
//        i.isHidden  = true
        i.addTarget(self, action: #selector(displayNote), for: .touchUpInside)
        return i
    }()
    
    
    @objc func displayNote(){
        guard let s = didSelectDisplayNote else {
            return
        }
        s()
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(self.timerView)
        self.timerView.snp.makeConstraints {
            $0.leading.trailing.top.equalTo(self)
            $0.bottom.equalTo(self.snp.bottom).inset(25)
        }
        
        self.timerView.addSubview(noteImageView)
        self.noteImageView.snp.makeConstraints {
            $0.centerX.centerY.equalTo(timerView)
            $0.width.height.equalTo(30)
        }
        
        

        self.timerView.addSubview(self.baseTimeLineView)
        self.baseTimeLineView.snp.makeConstraints {
            $0.leading.top.bottom.equalTo(self.timerView)
            $0.width.equalTo(1)
        }
        
        self.timerView.addSubview(self.timeLabel)
        self.timeLabel.snp.makeConstraints {
            $0.leading.equalTo(self.snp.leading)
            $0.top.equalTo(self.timerView.snp.bottom).inset(-2)
            $0.width.lessThanOrEqualTo(100)
            $0.height.equalTo(11)

        }
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}
