//
//  HomeController+FolderOptions.swift
//  VoiceNote
//
//  Created by Adel Radwan on 3/3/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import UIKit


extension HomeController {
    
    
    func addOrUpdateFolderName(text:String = "",completion:@escaping (_ folderName:String)->Void){
        
        let alert = UIAlertController(title: "folder name", message: "Enter a text", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.text = text
            textField.placeholder = "Folder name"
        }
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
            guard let text = alert?.textFields![0].text else { return }
            completion(text)
        }))
        alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    
    func showActionSheet(deleteFolder:@escaping()->Void = {},changeFolderName:@escaping()->Void = {}){
        let alert = UIAlertController(title: "Select from menu", message: nil, preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "delete folder", style: .default) { _ in
            deleteFolder()
        }
        let updateAction = UIAlertAction(title: "change folder name", style: .default) { _ in
            changeFolderName()
        }
        
        
        alert.addAction(updateAction)
        alert.addAction(deleteAction)
        alert.addAction(UIAlertAction(title: "colse", style: .cancel, handler: nil))
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
}
