//
//  FolderCell.swift
//  VoiceNote
//
//  Created by Adel Radwan on 3/3/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import UIKit

class FolderCell : UICollectionViewCell {
    
    var didSelectFolderSettigs : (()->())?
    
    private lazy var folderIcon : UIImageView = {
        let c = UIImageView(image: #imageLiteral(resourceName: "folder"))
        c.tintColor = .white
        return c
    }()
    
    lazy var folderSettingsBtn : UIButton = {
        let b = UIButton(type: .system)
        b.setImage(#imageLiteral(resourceName: "folderSettings"), for: .normal)
        b.tintColor = .white
        b.addTarget(self, action: #selector(folderSettings), for: .touchUpInside)
        return b
    }()

    
    private lazy var folderName : UILabel = {
        let l = UILabel()
        l.textColor = .white
        l.text = "Folder name"
        return l
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = .blueColor
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
        
        addSubview(self.folderIcon)
        self.folderIcon.snp.makeConstraints {
            $0.leading.top.equalTo(self).inset(16)
            $0.width.height.equalTo(30)
        }
        
        addSubview(self.folderSettingsBtn)
        self.folderSettingsBtn.snp.makeConstraints {
            $0.trailing.top.equalTo(self).inset(16)
            $0.width.height.equalTo(30)
        }
        
        addSubview(self.folderName)
        self.folderName.snp.makeConstraints {
            $0.leading.trailing.equalTo(self).inset(16)
            $0.top.equalTo(self.folderIcon.snp.bottom).inset(-12)
            $0.bottom.equalTo(self.snp.bottom).inset(12)
        }
    }

    
    
    func configData(folderName:String){
        self.folderName.text = folderName
    }
    
   @objc private func folderSettings(){
        guard let s =  didSelectFolderSettigs else{
            return
        }
        s()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
