//
//  Theams.swift
//  VoiceNote
//
//  Created by Adel Radwan on 2/20/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var blueColor : UIColor {
        return UIColor(red:0.00, green:0.36, blue:0.62, alpha:1.0)
    }
    
}


class Constant {
    
    
    enum HighlightColor :String {
        case redColor     = "red"
        case yellowColor  = "yellow"
        case orangeColor  = "orange"
        case defualtColor = "defualt"
    }
    
    static var highlightColor:HighlightColor = .defualtColor
    
    
    
    static func getHighlight(_ color:HighlightColor)->UIColor{
        switch color {
        case.redColor    : return .red
        case.yellowColor : return .yellow
        case.orangeColor : return .orange
        case.defualtColor: return UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.0)
        }
    }
    
    
}
