//
//  HomeControllerViewController.swift
//  VoiceNote
//
//  Created by Adel Radwan on 3/3/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import UIKit
import RealmSwift

class HomeController: UIViewController {
    
    var notificationToken : NotificationToken?
    lazy var folderData:Results<AudioFolder> = {
        return RealmService.shared.realm.objects(AudioFolder.self).sorted(byKeyPath: "createdAt", ascending: false)
    }()
    
    lazy var colletionView:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 10
        layout.minimumInteritemSpacing = 0
        layout.sectionInset = UIEdgeInsets(top: 15, left: 10, bottom: 0, right: 10)
        let c = UICollectionView(frame: .zero, collectionViewLayout: layout)
        c.dataSource = self
        c.delegate   = self
        c.backgroundColor = .white
        c.register(FolderCell.self, forCellWithReuseIdentifier: "cell")
        return c
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupViews()
        self.setupNavigationItem()
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.updateRealm()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.stopRealmUpdate()
    }
    
    
    @objc func addNewFolder(){
        
        self.addOrUpdateFolderName { name in
            let model = AudioFolder(fileName: name)
            model.saveToRealmWithUpdate()
        }
        
    }
    
    
}



