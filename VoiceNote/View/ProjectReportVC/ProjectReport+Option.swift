//
//  ProjectReport+Option.swift
//  Samam
//
//  Created by Adel Radwan on 2/7/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import UIKit

extension ProjectReportVC {
    
    
    
    
    private func blackView()->UIView {
        let blackView = UIView()
        blackView.alpha = 0
        blackView.backgroundColor = .black
        return blackView
    }
    
    func show(){
        print(second)
        guard let keyWindow = UIApplication.shared.keyWindow else { return }
        
        self.blView = blackView()
        
        blView.frame = keyWindow.frame
        keyWindow.addSubview(blView)
        
        self.frame = CGRect(x: 10, y: -140, width: keyWindow.frame.width - 20, height: 250)
        keyWindow.addSubview(self)
        
        UIView.animate(withDuration: 0.75, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.blView.alpha = 0.6
            self.frame = CGRect(x: 10, y: (keyWindow.frame.height - 250) / 2, width: keyWindow.frame.width - 20, height: 250)
            
        },completion: { _ in
            
            
            NotificationCenter.keyboardWillShow(self, selector: #selector(self.keyboardWillShow(_:)))
            NotificationCenter.keyboardWillHide(self, selector: #selector(self.keyboardWillHide(_:)))
            self.reportTextView.becomeFirstResponder()
            
        })
        
    }
    
    
    func close(){
        
        self.endEditing(true)
        guard let keyWindow = UIApplication.shared.keyWindow else { return }
        UIView.animate(withDuration: 0.75, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.frame = CGRect(x: 10, y: keyWindow.frame.maxY, width: keyWindow.frame.width - 20, height: 250)
            self.blView.alpha = 0
            
        },completion: { _ in
            //            self.rootVC?.isEnableIQKeyboard(false)
            self.blView.removeFromSuperview()
            self.removeFromSuperview()
            NotificationCenter.endObservation(self)
            
        })
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            guard let s = self.didSelectColseBtn else { return }
            s()
        }
        
    }
    
    
}

//MARK: - KeyoardObservation
extension ProjectReportVC {
    
    @objc private func keyboardWillShow(_ not : Notification){
        let frame = not.keyboardFrame!
        let duration = not.keyboardDuration!
        guard let keyWindow = UIApplication.shared.keyWindow else { return }
        
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.frame = CGRect(x: 10, y: (frame.height - 90), width: keyWindow.frame.width - 20, height: 250)
        })
        
    }
    
    @objc private func keyboardWillHide(_ not : Notification){
        let duration = not.keyboardDuration!
        guard let keyWindow = UIApplication.shared.keyWindow else { return }
        
        UIView.animate(withDuration: duration, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.frame = CGRect(x: 10, y: (keyWindow.frame.height - 250) / 2, width: keyWindow.frame.width - 20, height: 250)
        })
        
    }
    
}



//MARK: - notificationCenter
extension NotificationCenter {
    
    class func keyboardWillShow(_ observ : Any,selector : Selector){
        self.default.addObserver(observ, selector: selector, name: UIResponder.keyboardWillShowNotification, object: nil)
        
    }
    class func keyboardWillHide(_ observ : Any, selector : Selector){
        self.default.addObserver(observ, selector: selector, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    
    class func endObservation(_ observ : Any){
        self.default.removeObserver(observ)
    }
}

//MARK: - this param we needed to detrmain keyboard frame , show & hide animation duration
extension Notification {
    
    var keyboardFrame : CGRect? {
        return (self.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
    }
    
    var keyboardDuration : TimeInterval? {
        return self.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval
    }
    
}
