//
//  AudioFile.swift
//  VoiceNote
//
//  Created by Adel Radwan on 2/18/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import UIKit
import RealmSwift


@objcMembers class AudioFolder:Object {
    
    dynamic var id          : String = ""
    dynamic var folderName  : String = ""
    dynamic var createdAt   : Date   = Date()
    
    var audioFile = List<AudioFile>()
    
    
    convenience init(fileName:String) {
        self.init()
        
        self.id = UUID().uuidString
        self.folderName = fileName

    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    
}

