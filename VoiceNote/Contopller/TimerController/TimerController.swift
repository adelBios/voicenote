//
//  TimerController.swift
//  VoiceNote
//
//  Created by Adel Radwan on 2/20/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import UIKit
import RealmSwift

class TimerController: UIViewController  {
    
    
    var folderId : String = ""
    
    var notificationToken : NotificationToken?
    lazy var folderData:Results<AudioFolder> = {
        return RealmService.shared.realm.objects(AudioFolder.self).filter("id = '\(folderId)'")
    }()
    
    
    lazy var noteView : ProjectReportVC = {
        let p = ProjectReportVC(frame: .zero)
        p.rootViewController = self
        p.rootVC = self
        return p
    }()
    
    
    var token :NotificationToken?
    lazy var timeArray : Results<SoundDataTemp> = {
        return RealmService.shared.realm.objects(SoundDataTemp.self)
    }()
    
    
    var viewModels  : [DPArrowMenuViewModel] = []
    
    var isHiglited   = false
    var isAutoScroll = true
    
    
    lazy var collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.itemSize = CGSize(width: 200, height: 80)
        let c = UICollectionView(frame: .zero, collectionViewLayout: layout)
        c.delegate = self
        c.dataSource = self
        c.backgroundColor = .white
        c.register(VoiceNoteCell.self, forCellWithReuseIdentifier: "cell")
        c.alwaysBounceHorizontal = true
        c.showsHorizontalScrollIndicator = false
        return c
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupViews()
        self.colorMenuOption()
        self.token = RealmService.shared.updateUI {
            self.collectionView.reloadData()
        }
    }
    
    
    // this func i called in HomeViewController in start record file
    func makeHiglited(completion:@escaping(_ val:Bool)->Void){
        if isHiglited == true {
            isHiglited = false
        }else {
            isHiglited = true
        }
        completion(self.isHiglited)
    }
    
    // this func i called in HomeViewController in start record file
    func showMenu(_ sender: UIBarButtonItem){
        
        let view = sender.value(forKey: "view") as? UIView
        DPArrowMenu.show(view ?? UIView(), viewModels: viewModels, done: { index in
            switch index {
            case 0:
                Constant.highlightColor = .redColor
            case 1:
                Constant.highlightColor = .orangeColor
            case 2:
                Constant.highlightColor = .yellowColor
            default:
                Constant.highlightColor = .redColor
            }
            
        }) {
            print("cansel Drop DownMenu")
        }
        
    }
    
    
    func updateCollectionView() {
        let indexPath = IndexPath(item: self.timeArray.count - 1, section: 0)
        if self.isAutoScroll == true {
            self.collectionView.scrollToItem(at: indexPath, at: .right, animated: true)
        }
    }
    
    
    func appendSoundData(_ time:String){
        let model = SoundDataTemp(time: time, isHiglighted: self.isHiglited, higlightedColor: Constant.highlightColor.rawValue, isMarked: false)
        
        model.saveToRealmWithUpdate()
        self.updateCollectionView()
        
    }
    
    
    func addMarker(){
        self.isAutoScroll = false
    }
    
}


