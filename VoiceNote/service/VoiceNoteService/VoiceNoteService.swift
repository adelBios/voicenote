//
//  Helper.swift
//  VoiceNote
//
//  Created by Adel Radwan on 2/18/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import UIKit
import AVFoundation


protocol RecorderViewControllerDelegate: class {
    func didStartRecording()
    func didFinishRecording()
}


class VoiceNoteService {
    
    private init(){}
    
    static let shared = VoiceNoteService()
    var audioEngine = AVAudioEngine()
    var audioFile  : AVAudioFile?

//    let settings = [AVFormatIDKey:kAudioFormatMPEG4AAC,
//                    AVSampleRateKey:Float64(44100),
//                    AVNumberOfChannelsKey:1,
//                    AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue] as [String : Any]
    
    
    let settings = [AVFormatIDKey: kAudioFormatLinearPCM, AVLinearPCMBitDepthKey: 16, AVLinearPCMIsFloatKey: true, AVSampleRateKey: Float64(44100), AVNumberOfChannelsKey: 1] as [String : Any]
    
     func format() -> AVAudioFormat? {
        let format = AVAudioFormat(settings: self.settings)
        return format
    }
}
