//
//  HomeViewController+.swift
//  VoiceNote
//
//  Created by Adel Radwan on 2/18/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import UIKit
import SnapKit
import Accelerate

extension RecorderController {
    
    
    
    func setupViews(){
        
        self.view.backgroundColor = .white
        
        view.addSubview(timerController.view)
        timerController.view.snp.makeConstraints {
            $0.leading.trailing.equalTo(self.view)
            $0.top.equalTo(self.view)
            $0.height.equalTo(80)
        }
        
        self.addChild(timerController)
        
        
        self.view.addSubview(self.audioView)
        self.audioView.snp.makeConstraints {
            $0.leading.equalTo(view.snp.leading)
            $0.top.equalTo(timerController.view.snp.bottom).inset(-10)
            $0.height.equalTo(100)
            $0.width.equalTo(self.view.snp.width)
        }
        
        view.addSubview(self.timeLabel)
        self.timeLabel.snp.makeConstraints {
            $0.centerX.equalTo(self.view.snp.centerX)
            $0.top.equalTo(self.audioView.snp.bottom).inset(-20)
        }
        
        self.view.addSubview(self.recordButton)
        self.recordButton.snp.makeConstraints {
            $0.centerX.equalTo(self.view.snp.centerX)
            $0.top.equalTo(self.timeLabel.snp.bottom).inset(-20)
            $0.width.height.equalTo(60)
        }
        
        view.addSubview(makeHighLitedBtn)
        makeHighLitedBtn.snp.makeConstraints {
            $0.leading.equalTo(self.view.snp.leading).inset(12)
            $0.top.bottom.equalTo(recordButton)
            $0.width.equalTo(150)
        }
        
        self.view.addSubview(makeNoteBtn)
        self.makeNoteBtn.snp.makeConstraints {
            $0.leading.equalTo(self.recordButton.snp.trailing).inset(-8)
            $0.trailing.equalTo(self.view.snp.trailing).inset(8)
            $0.top.bottom.equalTo(recordButton)
        }
        
        self.view.addSubview(self.showRecordFile)
        self.showRecordFile.snp.makeConstraints {
           $0.leading.trailing.equalTo(self.view).inset(30)
            $0.top.equalTo(self.recordButton.snp.bottom).inset(-10)
            $0.height.equalTo(35)
        }
        
        
        view.addSubview(self.documentView)
        self.documentView.snp.makeConstraints {
            $0.leading.trailing.bottom.equalTo(view)
            $0.top.equalTo(self.showRecordFile.snp.bottom).inset(-5)
        }
        
    }

    
}
