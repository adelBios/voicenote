//
//  ProjectReportVCViewController.swift
//  Samam
//
//  Created by adel radwan on 11/8/18.
//  Copyright © 2018 adel radwan. All rights reserved.
//

import UIKit
import RealmSwift

class ProjectReportVC: UIView,UITextViewDelegate {
    
    
    lazy var timeArray : Results<SoundDataTemp> = {
        return RealmService.shared.realm.objects(SoundDataTemp.self).filter("time = '\(self.second)'")
    }()
    
    var didSelectColseBtn : (()->())?
    
    var isUpdateNoteView = false
    var second = ""
    var blView             : UIView!
    weak var rootVC        : UIViewController?
    var rootViewController : UIViewController?

    
    lazy var addReportButton : UIButton = {
        let b = UIButton(type: .system)
        b.setTitle("add note".uppercased(), for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.backgroundColor  = .blueColor
        b.addTarget(self, action: #selector(self.addNote), for: .touchUpInside)
        return b
    }()
    
    
    
    
    @objc func addNote(){
        
        guard let text = self.reportTextView.text else {
            return
        }
        let realm = RealmService.shared.realm
        let model = self.timeArray.first
        
        if self.isUpdateNoteView == true {
            
            
            try! realm.write {
                model?.note.first?.note = text
            }
            
            
            
        }else{
            
            let noteModel = SoundDataNoteTemp(time: self.second, note: text)
            try! realm.write {
                model?.isMarked = true
                model?.note.append(noteModel)
            }
        }
        
        
//        self.reportTextView.text = ""
        
        self.close()
      
        
    }
    
    
    
   @objc func collaps(){
        self.close()
    }
    
    
    lazy var closeButton : UIButton = {
        let b = UIButton(type: .system)
        b.setTitle("close".uppercased(), for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.backgroundColor  = .lightGray
        b.addTarget(self, action: #selector(self.collaps), for: .touchUpInside)
        return b
    }()
    
    lazy var stackView : UIStackView = {
        let s = UIStackView(arrangedSubviews: [self.closeButton,self.addReportButton])
        s.axis = .horizontal
        s.distribution = .fillEqually
        s.spacing = 0
        return s
    }()
    
    
    lazy var reportLabel : UILabel = {
        let l = UILabel()
        l.text = "here..... add the note to this mark"
        l.textColor = .gray
        l.textAlignment = .justified
        return l
    }()
    
    
    lazy var reportTextView : UITextView = {
        let t = UITextView()
        t.delegate = self
        t.textAlignment = .justified
        t.textColor = .black
        t.font = self.reportLabel.font
        t.autocorrectionType = .no
        return t
    }()
    
    
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.isEmpty {
            self.reportLabel.isHidden = false
        }else{
            self.reportLabel.isHidden = true
        }
        
    }
    
    deinit {
        print("reomve")
    }
    
    
   override init(frame: CGRect) {
        super.init(frame: frame)
    
        self.setupUI()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
