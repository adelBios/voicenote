//
//  HomeViewController.swift
//  VoiceNote
//
//  Created by Adel Radwan on 2/18/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import UIKit
import AVFoundation
import Accelerate
import QuickLook
import RealmSwift

class RecorderController: UIViewController ,RecorderViewControllerDelegate{
    
    
    var folderId : String = ""
    var audioData = Data()
    var notificationToken : NotificationToken?
    lazy var folderData:Results<AudioFolder> = {
        return RealmService.shared.realm.objects(AudioFolder.self).filter("id = '\(folderId)'")
    }()
    
    
    lazy var noteView : ProjectReportVC = {
        let p = ProjectReportVC(frame: .zero)
        p.rootViewController = self
        p.rootVC = self
        return p
    }()

    
    let settings = [AVFormatIDKey: kAudioFormatLinearPCM, AVLinearPCMBitDepthKey: 16, AVLinearPCMIsFloatKey: true, AVSampleRateKey: Float64(44100), AVNumberOfChannelsKey: 1] as [String : Any]
    let audioEngine = AVAudioEngine()
     var renderTs: Double = 0
     var recordingTs: Double = 0
     var silenceTs: Double = 0
     var audioFile: AVAudioFile?
    var fileUrl = [URL]()
    var audioUrl : URL!
    weak var delegate: RecorderViewControllerDelegate?
    

    var audioView : AudioVisualizerView = {
        let v = AudioVisualizerView()
        v.backgroundColor = .black
        return v
    }()
    
    var recordButton : RecordButton = {
        let b = RecordButton()
        b.isRecording = false
        b.addTarget(self, action: #selector(handleRecording(_:)), for: .touchUpInside)
        return b
    }()
    
    
    var timeLabel : UILabel = {
        let l = UILabel()
        l.text = "00:00"
        l.textColor = UIColor(red:0.00, green:0.36, blue:0.62, alpha:1.0)
        return l
    }()
    
    lazy var makeHighLitedBtn : UIButton = {
        let l = UIButton(type: .system)
        l.setTitle("Highlight", for: .normal)
        l.setImage(UIImage(named: "highlight"), for: .normal)
        l.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
        l.tintColor = .blueColor
        l.setTitleColor(.blueColor, for: .normal)
        l.addTarget(self, action: #selector(self.makeHiglited(_:)), for: .touchUpInside)
        return l
    }()
    
    
    lazy var makeNoteBtn : UIButton = {
        let l = UIButton(type: .system)
        l.setTitle("add note", for: .normal)
        l.setImage(#imageLiteral(resourceName: "marker"), for: .normal)
        l.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 5)
        l.tintColor = .blueColor
        l.setTitleColor(.blueColor, for: .normal)
        l.addTarget(self, action: #selector(self.addMarkNote(_:)), for: .touchUpInside)
        return l
    }()
    
    
    lazy var showRecordFile : UIButton = {
        let l = UIButton(type: .system)
        l.setTitle("Show recorder file", for: .normal)
        l.setTitleColor(.blueColor, for: .normal)
        l.addTarget(self, action: #selector(self.showRecoderController), for: .touchUpInside)
        return l
    }()
    
    
    
    lazy var  previewController : QLPreviewController = {
        let p = QLPreviewController()
        p.view.backgroundColor = .white
        p.dataSource = self
        return p
    }()
    
    lazy var timerController : TimerController = {
        let t = TimerController()
        t.folderId = self.folderId
        return t
    }()
    
    let documentView : UIView = {
        let v = UIView()
        return v
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Home"
        self.delegate = self
        self.setupVoiceService()
        self.setupViews()
        self.setupNavigationItems()
        
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.audioObserv()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.endAudioObserv()
    }
    
    
     func format() -> AVAudioFormat? {
        let format = AVAudioFormat(settings: self.settings)
        return format
    }
    
    func setupVoiceService(){
        
        let shared = VoiceNoteService.shared
        shared.audioEngine = self.audioEngine
        shared.audioFile   = self.audioFile
    }
    
    
    @objc private func showRecoderController(){
        let vc = RecordingsViewController()
//        vc.folderId = self.folderId
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}


//MARK: - checkPermissionAndRecord
extension RecorderController {
    
    func checkPermissionAndRecord() {
        let permission = AVAudioSession.sharedInstance().recordPermission
        switch permission {
        case .undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission({ (result) in
                DispatchQueue.main.async {
                    if result {
                        self.startRecording()
                    }
                }
            })
            break
        case .granted:
            self.startRecording()
            break
        case .denied:
            break
        }
    }
    
}


//MARK: - stop Recording
extension RecorderController {
    
    
    func stopRecording() {
      
        if let d = self.delegate {
            d.didFinishRecording()
        }
        
        self.audioFile = nil
        self.audioEngine.inputNode.removeTap(onBus: 0)
        self.audioEngine.stop()
        
        do {
            try AVAudioSession.sharedInstance().setActive(false)
            
        } catch  let error as NSError {
            print(error.localizedDescription)
            return
        }
  
    }
    
    
    private func storeRecoding(_ realm: Realm) {
        
        let a = createAudioRecordFile()

        let audioFileModel  = AudioFile(filePath: a!.url.absoluteString,parentId:self.folderId)
        
        try! realm.write {
            self.folderData.first!.audioFile.append(audioFileModel)
        }
        
        
        let object = realm.objects(SoundDataTemp.self)
        
        object.forEach { m in
            
            let model = SoundData(audioFileId:audioFileModel.id,time: m.time, isHiglighted: m.isHiglighted, higlightedColor: m.higlightedColor, isMarked: m.isMarked)
            
            
            try! realm.write {
                model.note = m.note
                audioFileModel.soundData.append(model)
            }
            
        }
    }
    
    
    func didStartRecording() {
        
    }
    
    func didFinishRecording() {
        let realm = RealmService.shared.realm
        self.storeRecoding(realm)
    }
}


