//
//  SoundData.swift
//  VoiceNote
//
//  Created by Adel Radwan on 3/3/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import UIKit
import RealmSwift


@objcMembers class SoundData:Object {
    
    
    dynamic var audioFileId     : String  = ""
    dynamic var time            : String  = ""
    dynamic var isHiglighted    : Bool    = false
    dynamic var higlightedColor : String  = ""
    dynamic var isMarked        : Bool    = false
    
    var note = List<SoundDataNoteTemp>()
    
    convenience init(audioFileId:String,time : String,isHiglighted:Bool,higlightedColor:String,isMarked :Bool) {
        self.init()
        
        self.audioFileId     = audioFileId
        self.time            = time
        self.isHiglighted    = isHiglighted
        self.isMarked        = isMarked
        self.higlightedColor = higlightedColor
        
    }
    
}

// why i Repeate this class ?! ======================
// SoundData Model i use it for preview sound with option when play sound
//AND ----- SoundDataTemp just for real time object binding when start record Voice
//Here we go baby..

@objcMembers class SoundDataTemp:Object {
    
    dynamic var time            : String  = ""
    dynamic var isHiglighted    : Bool    = false
    dynamic var higlightedColor : String  = ""
    dynamic var isMarked        : Bool    = false
    
    var note = List<SoundDataNoteTemp>()
    
    
    convenience init(time : String,isHiglighted:Bool,higlightedColor:String,isMarked :Bool) {
        self.init()
        
        self.time            = time
        self.isHiglighted    = isHiglighted
        self.isMarked        = isMarked
        self.higlightedColor = higlightedColor
        
    }
    
    
    override static func primaryKey() -> String? {
        return "time"
    }
    
    
}


@objcMembers class SoundDataNoteTemp : Object {
    
    
    dynamic var time : String  = ""
    dynamic var note : String  = ""
    
    convenience init(time : String,note:String) {
        self.init()
        
        self.time = time
        self.note = note
        
    }
    
    
}




