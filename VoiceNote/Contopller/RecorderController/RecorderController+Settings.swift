//
//  HomeViewController+Settings.swift
//  VoiceNote
//
//  Created by Adel Radwan on 2/21/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import UIKit
import AVFoundation

extension RecorderController {
    
    //MARK: - ========== Function ================
    func setupNavigationItems(){
        
        let addFile = UIBarButtonItem(title: "Add file", style: .plain, target: self, action: #selector(self.showDocumentPicker))
        
        let highlight = UIBarButtonItem(title: "Highlight color", style: .plain, target: self, action: #selector(self.showColorMenu(_:)))
        
        self.navigationItem.rightBarButtonItems = [addFile,highlight]
    }
    
    
    func audioObserv(){
        let notificationName = AVAudioSession.interruptionNotification
        NotificationCenter.default.addObserver(self, selector: #selector(handleRecording(_:)), name: notificationName, object: nil)
    }
    
    func endAudioObserv(){
        NotificationCenter.default.removeObserver(self)
    }
    
    
    
    @objc func handleRecording(_ sender: RecordButton) {
        if recordButton.isRecording {
            self.checkPermissionAndRecord()
        } else {
            self.stopRecording()
        }
    }
    
    
    @objc  func showColorMenu(_ sender:UIBarButtonItem) {
        self.timerController.showMenu(sender)
    }
    
    
    @objc func makeHiglited(_ sender:UIButton) {
        self.timerController.makeHiglited {
            switch $0 {
            case true : sender.setTitle("Stop highlight", for: .normal)
                Constant.highlightColor = .redColor
            case false: sender.setTitle("Start highlight", for: .normal)
                Constant.highlightColor = .defualtColor
            }
        }
    }
    
    
    
    @objc private func showDocumentPicker(){
        
        let types = ["public.item", "public.folder", "public.directory"]
        let picker = UIDocumentPickerViewController(documentTypes: types, in: .import)
        
        picker.allowsMultipleSelection = false
        picker.delegate = self
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.present(picker, animated: true, completion: nil)
        }
        
    }
    
    
    @objc func addMarkNote(_ sender:UIButton) {
        self.timerController.addMarker()
        noteView.show()
        noteView.isUpdateNoteView = false
        noteView.second = self.timeLabel.text ?? "0.1"
        noteView.didSelectColseBtn = {
            self.timerController.isAutoScroll = true
        }
 
    }
    
    
    
}
