//
//  TimeView.swift
//  VoiceNote
//
//  Created by Adel Radwan on 2/20/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import UIKit

class TimeView: UIView {
    
    var timerView:UIView = {
        let v = UIView()
        v.backgroundColor = .lightGray
        return v
    }()
    
    private var baseTimeLineView:UIView = {
      let v = UIView()
        v.backgroundColor = .black
        return v
    }()

 
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(self.timerView)
        self.timerView.snp.makeConstraints {
            $0.leading.trailing.top.equalTo(self)
            $0.bottom.equalTo(self.snp.bottom).inset(25)
        }
        
        self.addSubview(self.baseTimeLineView)
        self.baseTimeLineView.snp.makeConstraints {
            $0.leading.top.bottom.equalTo(self)
            $0.bottom.equalTo(self.snp.bottom).inset(20)
            $0.width.equalTo(1)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    


}
