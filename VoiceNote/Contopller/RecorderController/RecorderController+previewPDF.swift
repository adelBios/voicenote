//
//  HomeViewController+previewPDF.swift
//  VoiceNote
//
//  Created by Adel Radwan on 2/20/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import UIKit
import QuickLook

extension RecorderController : QLPreviewControllerDataSource {
    
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return self.fileUrl.count
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return NSURL(fileURLWithPath: self.fileUrl.first!.path)
    }
    
}
