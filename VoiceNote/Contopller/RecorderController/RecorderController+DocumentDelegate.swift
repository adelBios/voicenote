//
//  HomeViewController+DocumentDelegate.swift
//  VoiceNote
//
//  Created by Adel Radwan on 2/20/19.
//  Copyright © 2019 adel radwan. All rights reserved.
//

import UIKit


extension RecorderController : UIDocumentPickerDelegate {
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let myURL = url as URL
        
        self.fileUrl.removeAll()
        self.fileUrl.append(myURL)
        
        self.documentView.addSubview(previewController.view)
        previewController.view.snp.makeConstraints {
            $0.leading.trailing.top.bottom.equalTo(self.documentView)
        }
    
        self.addChild(self.previewController)
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.previewController.reloadData()
        }
        
    }
    
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        dismiss(animated: true, completion: nil)
    }
    
}
